export enum UsersRole
{
    ADMIN = 'administrateur',
    VISITOR = 'visiteur',
    COLLABORATOR = 'collaborateur',
    READER = 'lecteur',
}