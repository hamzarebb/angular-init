import 
{ 
  Component, 
  OnDestroy, 
  OnInit 
} from '@angular/core'
import { UserType } from './types/user-type'
import { UsersRole } from './enums/roles.enum'
import { SelectedType } from './types/selected-type'

@Component
({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})

export class AppComponent implements OnInit, OnDestroy 
{
  readonly title: string = 'angular-init'

  counter: number = 0
  private counterInterval: any

  users: Array<UserType & SelectedType> = [
    {
      id: 1,
      lastname: 'Doe',
      firstname: 'John',
      role: UsersRole.ADMIN,
    },
    {
      id: 2,
      lastname: 'Doe',
      firstname: 'Jane',
      role: UsersRole.VISITOR,
    },
    {
      id: 3,
      lastname: 'Doe',
      firstname: 'Jim',
      role: UsersRole.COLLABORATOR,
    },
    {
      id: 4,
      lastname: 'Doe',
      firstname: 'Joe',
      role: UsersRole.READER,
    }
  ]

  showPassword = false
  isBtnDisabled = false

  ngOnInit(): void 
  {
    this.counterInterval = setInterval
    (
      () => this.counter++, 
      1000
    )
  }

  ngOnDestroy(): void 
  {
    clearInterval(this.counterInterval)
  }
  
  toggleShowPassword(): void
  {
    this.showPassword = true
    this.isBtnDisabled = true

    setTimeout(() => 
    {
      this.showPassword = false
      this.isBtnDisabled = false
    }, 1000)
  }

  allSelected(): boolean
  {
    return this.users.every(user => user.selected)
  }

  allSelectedManually(): boolean
  {
    return this.users.some(user => user.selected) && !this.allSelected()
  }

  selectedCount()
  {
    return this.users.filter(user => user.selected).length
  }

  toggleSelectAll(): void
  {
    const selectAll = !this.allSelected()
    this.users.forEach(user => user.selected = selectAll)
  }

  deleteUser(user: UserType): void
  {
    const index = this.users.indexOf(user)
    if (index > -1) 
    {
      this.users.splice(index, 1)
    }
  }
}
