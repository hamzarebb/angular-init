export type UserType = 
{
    id: number
    lastname: string
    firstname: string
    role: string
}